import os

import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns

import xpresso.ai.core.data.visualization.utils as utils
from xpresso.ai.core.commons.utils.constants import EMPTY_STRING
from xpresso.ai.core.data.automl.utils import get_file_from_path_string, \
    set_extension


class Plot:
    def __init__(self):
        sns.set()
        self.plot = None
        self.axes_labels = dict()
        self.axes_labels["x_label"] = EMPTY_STRING
        self.axes_labels["y_label"] = EMPTY_STRING
        self.axes_labels["target_label"] = EMPTY_STRING
        self.plot_title = EMPTY_STRING
        self.figure_size = (15, 10)
        plt.figure(figsize=self.figure_size)

    @staticmethod
    def render(output_format=utils.HTML, output_path=utils.DEFAULT_IMAGE_PATH,
               file_name=None, bbox_inches=None):
        mpl.rcParams.update({'font.size': 22})

        if output_format == utils.HTML:
            if utils.detect_notebook():
                plt.show()
                return
        if output_format == utils.PNG:
            if file_name is None:
                file_name = EMPTY_STRING
            file_name = "{}_{}".format(file_name, utils.generate_id())
            output_path, file_name = get_file_from_path_string(output_path,
                                                               file_name)
            file_name = set_extension(file_name, utils.PNG)
            utils.make_folder(output_path)
            plt.tight_layout()
            plt.savefig(os.path.join(output_path, file_name),
                        bbox_inches=bbox_inches)
        plt.grid(False)
        plt.clf()
        plt.close('all')
